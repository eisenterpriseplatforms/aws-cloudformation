**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

**** DISCLAIMER ****

The EIS Enterprise Platforms Cloudformation templates contained in this repository are provided "as is". They are intended to provide an easy starting point for deploying simple applications and are intended to be modified to suit the purposes or users.

## Pre-Requisites for using Cloudformation

1. You must have access to an AWS account to run CloudFomration in
2. You must be authorized to deploy infrastructure into AWS

---

## Deploying a Cloudformation Template (general instructions)

1. Clone the Cloudformation repo, or copy down the template that you wish to deploy
2. Login to the AWS console as a user with permission to deploy Cloudformation templates
3. Go to the "Cloudformation" services page
4. Click the "Create Stack" button
5. Select the "Upload a Template to Amazon S3"
6. Click the "Choose File" button
7. Browse to the file representing your desired infrastructure
8. Fill in all the required form data
9. Click "Next"
10. Assign any tags to your instances that you'd like (recommended is to assign a "Name" tag with an indication of what you are deploying)
11. Click "Next"
12. Complete the preview and deploy
13. Once the Cloudformation stack has completed deployment, check the "Outputs" tab for important information

---

## General Info: Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You�ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you�d like to and then click **Clone**.
4. Open the directory you just created to see your repository�s files.
